import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetRelatedOrganizationComponent } from './src/base-widget.component';

export * from './src/base-widget.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	BaseWidgetRelatedOrganizationComponent
  ],
  exports: [
    BaseWidgetRelatedOrganizationComponent
  ]
})
export class RelatedOrganizationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RelatedOrganizationModule,
      providers: []
    };
  }
}
