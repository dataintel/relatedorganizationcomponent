import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relatedorganization',
  template: `<div class="container" style="padding: 10px">
    <div class="col-md-4">
        <h3 class="red">Related Organizations</h3>
        <hr>
        <small *ngFor = "let items of data">{{items.related_organizations}}, </small> 
    </div>
</div>
 `,
  styles: [`.red {
    color: red;
    font-weight: bold;
}

.col-md-4 small{
    color: #bbb7b7;
    text-transform: capitalize;
}`]
})

export class BaseWidgetRelatedOrganizationComponent {

    public data: Array<any> = [
    {"related_organizations":"Afrikaans"},
    {"related_organizations":"العربية"},
    {"related_organizations":"Беларуская"},
    {"related_organizations":"Català"},
    {"related_organizations":"Deutsch"},
    {"related_organizations":"Español"},
    {"related_organizations":"Esperanto"},
    {"related_organizations":"Euskara"},
    {"related_organizations":"Français"},
    {"related_organizations":"हिन्दी"},
    {"related_organizations":"Hrvatski"},
    {"related_organizations":"Italiano"},
    {"related_organizations":"Lietuvių"},
    {"related_organizations":"Nederlands"},
    {"related_organizations":"日本語"},
    {"related_organizations":"Português"},
    {"related_organizations":"Română"},
    {"related_organizations":"Русский"},
    {"related_organizations":"Slovenščina"}];

}
